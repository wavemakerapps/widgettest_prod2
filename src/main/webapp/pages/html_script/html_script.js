Application.$controller("html_scriptPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.text3Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.html1.hint = $isolateScope.datavalue;
    };

    $scope.toggle1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.html1.show = $isolateScope.datavalue;
    };

    $scope.textarea1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.html1.content = $isolateScope.datavalue;
    };

    $scope.text10Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.html1.width = $isolateScope.datavalue;
    };

    $scope.text4Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.html1.height = $isolateScope.datavalue;
    };

    $scope.text5Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.html1.horizontalalign = $isolateScope.datavalue;
    };

}]);