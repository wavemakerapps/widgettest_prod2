Application.$controller("anchor_events_partialPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.anchor1Doubletap = function($event, $isolateScope) {
        //$isolateScope.width = "500px";
        $isolateScope.caption = "Double tap";
        console.log("Double tap");
    };
    $scope.anchor1Tap = function($event, $isolateScope) {
        //$isolateScope.height = "500px";
        $isolateScope.hint = $scope.Widgets.text3.datavalue;
        $isolateScope.iconclass = $scope.Widgets.text5.datavalue;
        $isolateScope.iconurl = $scope.Widgets.text6.datavalue;
        console.log("Tap");
        //$isolateScope.hyperlink = "https://www.google.com/";
    };
    $scope.anchor1Mouseenter1 = function($event, $isolateScope) {
        //$scope.Actions.notificationAction1.invoke();
        //Need to bind variables 
        $isolateScope.caption = "Mouse enter extra";
        console.log("Mouse enter extra");
    };
    $scope.anchor1Click = function($event, $isolateScope) {
        //debugger;
        $isolateScope.caption = $scope.Widgets.text1.datavalue;
        $isolateScope.badgevalue = $scope.Widgets.text2.datavalue;
        $isolateScope.iconwidth = $scope.Widgets.text14.datavalue;
        $isolateScope.iconheight = $scope.Widgets.text14_1.datavalue;
        $isolateScope.iconposition = $scope.Widgets.select3.datavalue;
        $isolateScope.iconmargin = $scope.Widgets.text15.datavalue;
        $isolateScope.target = $scope.Widgets.select2.datavalue;
        $isolateScope.encodeurl = $scope.Widgets.text13.datavalue;
        console.log("Click");
        alert("AchorClick");
    };
    $scope.anchor1Dblclick = function($event, $isolateScope) {
        $isolateScope.caption = "Double click";
        $isolateScope.badgevalue = "Anchor Double";
        console.log("Double click");
    };
    $scope.anchor1Mouseenter = function($event, $isolateScope) {
        $isolateScope.caption = "Mouse enter";
        $isolateScope.hint = "Anchor MouseEnter";
        console.log("Mouse enter");
    };
    $scope.anchor1Mouseleave = function($event, $isolateScope) {
        $isolateScope.caption = "Mouse leave";
        $isolateScope.iconclass = "wi wi-alarm";
        console.log("Mouse leave");
    };
    $scope.anchor1Focus = function($event, $isolateScope) {
        $isolateScope.caption = "Focus";
        $isolateScope.iconposition = "right";
        console.log("Focus");
    };
    $scope.anchor1Blur = function($event, $isolateScope) {
        $isolateScope.caption = "";
        $isolateScope.badgevalue = "Blur";
        $isolateScope.iconposition = "top";
        console.log("Blur");
    };

}]);