Application.$controller("picture_events_partialPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.picture1Doubletap = function($event, $isolateScope) {
        $isolateScope.shape = "circle";
        $isolateScope.pictureaspect = "None";
        console.log("Double Tap");
    };
    $scope.picture1Tap = function($event, $isolateScope) {
        //$isolateScope.show = $scope.Widgets.toggle1.datavalue;
        $isolateScope.picturesource = $scope.Widgets.select1.datavalue;
        $isolateScope.pictureplaceholder = $scope.Widgets.select2.datavalue;
        console.log("Tap");
    };
    $scope.picture1Mouseleave = function($event, $isolateScope) {
        $isolateScope.pictureaspect = "V";
        $isolateScope.shape = "rounded";
        $isolateScope.picturesource = $scope.Widgets.select1.datavalue;
        console.log("Mouse Leave");
    };
    $scope.picture1Mouseenter = function($event, $isolateScope) {
        $isolateScope.pictureaspect = "Both";
        $isolateScope.shape = "thumbnail";
        $isolateScope.picturesource = $scope.Widgets.select1.datavalue;
        console.log("Mouse Enter");
    };
    $scope.picture1Dblclick = function($event, $isolateScope) {
        $isolateScope.shape = "circle";
        $isolateScope.picturesource = $scope.Widgets.select1.datavalue;
        console.log("Double Click");
    };
    $scope.picture1Click = function($event, $isolateScope) {
        //debugger;
        $isolateScope.hint = $scope.Widgets.text3.datavalue;
        $isolateScope.endodeurl = $scope.Widgets.toggle2.datavalue;
        $isolateScope.pictureaspect = $scope.Widgets.select3.datavalue;
        $isolateScope.shape = $scope.Widgets.select4.datavalue;
        /*$isolateScope.width = "500px";
          $isolateScope.height = "500px";*/

        console.log("Click");
    };
}]);