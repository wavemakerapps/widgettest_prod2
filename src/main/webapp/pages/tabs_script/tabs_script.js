Application.$controller("tabs_scriptPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.text1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.tabpane2.title = $isolateScope.datavalue;
    };

    $scope.text3Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.tabpane2.badgevalue = $isolateScope.datavalue;
    };

    $scope.select1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.tabpane2.badgetype = $isolateScope.datavalue;
    };

    $scope.toggle1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.tabpane2.show = $isolateScope.datavalue;
    };

    $scope.text5Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.tabpane2.paneicon = $isolateScope.datavalue;
    };

    $scope.text6Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.tabpane2.content = $isolateScope.datavalue;
    };

    $scope.toggle3Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.tabs1.show = $isolateScope.datavalue;
    };

    $scope.text18Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.tabs1.defaultpaneindex = $isolateScope.datavalue;
    };

    $scope.toggle4Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.tabpane2.disabled = $isolateScope.datavalue;
    };

    $scope.toggle2Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.tabpane2.deferload = $isolateScope.datavalue;
    };

    $scope.text11Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.tabs1.height = $isolateScope.datavalue;
    };

    $scope.text11_1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.tabs1.tabsposition = $isolateScope.datavalue;
    };

    $scope.text12Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.tabs1.transition = $isolateScope.datavalue;
    };

    $scope.text14Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.tabs1.horizontalalign = $isolateScope.datavalue;
    };

    $scope.toggle5Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.tabs1.deferload = $isolateScope.datavalue;
    };

}]);