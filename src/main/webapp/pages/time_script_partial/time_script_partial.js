Application.$controller("time_script_partialPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.text1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.time1.placeholder = $isolateScope.datavalue;
    };

    $scope.text3Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.time1.hint = $isolateScope.datavalue;
    };

    $scope.toggle1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.time1.show = $isolateScope.datavalue;
    };

    $scope.text5Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.time1.datavalue = $isolateScope.datavalue;
    };

    $scope.select1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.time1.timepattern = $isolateScope.datavalue;
    };

    $scope.select2Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.time1.outputformat = $isolateScope.datavalue;
    };

    $scope.toggle3Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.time1.required = $isolateScope.datavalue;
    };

    $scope.toggle4Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.time1.disabled = $isolateScope.datavalue;
    };

    $scope.toggle6Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.time1.readonly = $isolateScope.datavalue;
    };

    $scope.toggle2Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.time1.deferload = $isolateScope.datavalue;
    };

    $scope.text11_1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.time1.hourstep = $isolateScope.datavalue;
    };

    $scope.text12Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.time1.minutestep = $isolateScope.datavalue;
    };

    $scope.toggle5Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.time1.autofocus = $isolateScope.datavalue;
    };

    $scope.text13Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.time1.mintime = $isolateScope.datavalue;
    };

    $scope.text14_1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.time1.maxtime = $isolateScope.datavalue;
    };

}]);