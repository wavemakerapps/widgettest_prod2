Application.$controller("checkboxset_events_partialPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.checkboxset1Ready = function($event, $isolateScope) {
        debugger;
        console.log("C Ready");
        $isolateScope.width = $scope.Widgets.text7.datavalue;
        $isolateScope.height = $scope.Widgets.text13.datavalue;
        $isolateScope.layout = $scope.Widgets.select2.datavalue;
        $isolateScope.dataset = $scope.Widgets.text12.datavalue;
        $isolateScope.usekeys = $scope.Widgets.text8_1.datavalue;
        $isolateScope.datafield = $scope.Widgets.text9.datavalue;
        $isolateScope.displayfield = $scope.Widgets.text10.datavalue;
        $isolateScope.displayexpression = $scope.Widgets.text11.datavalue;
        $isolateScope.orderby = $scope.Widgets.text12_1.datavalue;
        $isolateScope.datavalue = $scope.Widgets.select1.datavalue;
        $isolateScope.show = $scope.Widgets.toggle5.datavalue;
        $isolateScope.readonly = $scope.Widgets.toggle1.datavalue;
        $isolateScope.disabled = $scope.Widgets.toggle3.datavalue;
    };
    $scope.checkboxset1Tap = function($event, $isolateScope) {
        console.log("C Tap");
        $isolateScope.layout = "inline";
    };
    $scope.checkboxset1Mouseleave = function($event, $isolateScope) {
        console.log("C Mleave");
        $isolateScope.displayexpression = "$[Zip]";
    };
    $scope.checkboxset1Mouseenter = function($event, $isolateScope) {
        console.log("C Menter");
        $isolateScope.layout = "stacked";
    };
    $scope.checkboxset1Click = function($event, $isolateScope) {
        console.log("C Click");
        $isolateScope.orderby = "Zip:asc";
    };
    $scope.checkboxset1Change = function($event, $isolateScope, newVal, oldVal) {
        console.log("Change: " + newVal + " changed from " + oldVal);
        $isolateScope.orderby = "dataValue:desc";
    };

}]);