Application.$controller("time_events_partialPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.time1Change = function($event, $isolateScope, newVal, oldVal) {
        console.log($event.type);
        console.log("Value changed to " + newVal + "from " + oldVal);
        $isolateScope.timepattern = "H:m:s"
        $isolateScope.outputformat = "hh:mm a"
    };


    $scope.time1Focus = function($event, $isolateScope) {
        console.log($event.type);
        $isolateScope.timepattern = "HH:mm:ss"
        $isolateScope.outputformat = "hh:mm"
    };


    $scope.time1Blur = function($event, $isolateScope) {
        console.log($event.type);
        $isolateScope.timepattern = "hh:mm:ss"
        $isolateScope.outputformat = "HH:mm"
    };


    $scope.time1Click = function($event, $isolateScope) {
        console.log($event.type);
        $isolateScope.datavalue = $scope.Widgets.text5.datavalue;
        $isolateScope.hint = $scope.Widgets.text3.datavalue;
        $isolateScope.timepattern = $scope.Widgets.select1.datavalue;
        $isolateScope.hourstep = $scope.Widgets.text11_1.datavalue;
        $isolateScope.minutestep = $scope.Widgets.text12.datavalue;
    };


    $scope.time1Mouseenter = function($event, $isolateScope) {
        console.log($event.type);
        $isolateScope.timepattern = "h:m:s"
        $isolateScope.outputformat = "hh:mm:ss a"
    };


    $scope.time1Mouseleave = function($event, $isolateScope) {
        console.log($event.type);
        $isolateScope.timepattern = "HH:mm:ss a"
        $isolateScope.outputformat = "HH:mm:ss"
    };


    $scope.time1Tap = function($event, $isolateScope) {
        console.log($event.type);
        $isolateScope.outputformat = $scope.Widgets.select2.datavalue;
        $isolateScope.mintime = $scope.Widgets.text13.datavalue;
        $isolateScope.maxtime = $scope.Widgets.text14_1.datavalue;
    };


}]);