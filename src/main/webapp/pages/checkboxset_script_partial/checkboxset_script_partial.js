Application.$controller("checkboxset_script_partialPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.text14_1Change = function($event, $isolateScope) {
        $scope.Widgets.checkboxset1.groupby = $isolateScope.datavalue;
    };
    $scope.text13Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.checkboxset1.height = $isolateScope.datavalue;
    };
    $scope.text7Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.checkboxset1.width = $isolateScope.datavalue;
    };
    $scope.text9Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.checkboxset1.datafield = $isolateScope.datavalue;
    };
    $scope.text10Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.checkboxset1.displayfield = $isolateScope.datavalue;
    };
    $scope.text11Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.checkboxset1.displayexpression = $isolateScope.datavalue;
    };
    $scope.text12_1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.checkboxset1.orderby = $isolateScope.datavalue;
    };
    $scope.text8_1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.checkboxset1.usekeys = $isolateScope.datavalue;
    };
    $scope.select2Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.checkboxset2.layout = $isolateScope.datavalue;
    };
    $scope.text7_1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.checkboxset7.datavalue = $isolateScope.datavalue;
    };
    $scope.toggle5Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.checkboxset2.show = $isolateScope.datavalue;
    };
    $scope.toggle3Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.checkboxset2.disabled = $isolateScope.datavalue;
    };
    $scope.toggle1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.checkboxset2.readonly = $isolateScope.datavalue;
    };
    $scope.select1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.checkboxset2.datavalue = $isolateScope.datavalue;
    };
    $scope.text12Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.checkboxset2.dataset = $isolateScope.datavalue;
    };

}]);