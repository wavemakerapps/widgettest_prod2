Application.$controller("picture_script_partialPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.text3Change = function($event, $isolateScope, newVal, oldVal) {
        console.log($scope);
        $scope.Widgets.picture1.hint = $isolateScope.datavalue;
    };

    $scope.toggle1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.picture1.show = $isolateScope.datavalue;
    };

    $scope.select1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.picture1.picturesource = $isolateScope.datavalue;
    };

    $scope.select2Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.picture1.pictureplaceholder = $isolateScope.datavalue;
    };

    $scope.toggle2Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.picture1.endodeurl = $isolateScope.datavalue;
    };

    $scope.select3Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.picture1.pictureaspect = $isolateScope.datavalue;
    };

    $scope.select4Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.picture1.shape = $isolateScope.datavalue;
    };

    $scope.text10Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.picture1.width = $isolateScope.datavalue;
    };

    $scope.text7_1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.picture1.height = $isolateScope.datavalue;
    };


    $scope.select1Change1 = function($event, $isolateScope, newVal, oldVal) {

    };

}]);