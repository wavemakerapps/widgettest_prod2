Application.$controller("iframedialog_script_partialPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.text3Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.iframedialog1.title = $isolateScope.datavalue;
    };
    $scope.text9Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.iframedialog1.oktext = $isolateScope.datavalue;
    };
    $scope.text12Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.iframedialog1.iconclass = $isolateScope.datavalue;
    };
    $scope.select1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.iframedialog1.url = $isolateScope.datavalue;
    };
    $scope.toggle1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.iframedialog1.modal = $isolateScope.datavalue;
    };
    $scope.toggle2Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.iframedialog1.closable = $isolateScope.datavalue;
    };
    $scope.toggle3Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.iframedialog1.showheader = $isolateScope.datavalue;
    };
    $scope.toggle4Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.iframedialog1.showactions = $isolateScope.datavalue;
    };
    $scope.toggle5Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.iframedialog1.encodeurl = $isolateScope.datavalue;
    };
    $scope.text12_1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.iframedialog1.width = $isolateScope.datavalue;
    };
    $scope.text9_1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.iframedialog1.height = $isolateScope.datavalue;
    };
    $scope.text10Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.iframedialog1.iconwidth = $isolateScope.datavalue;
    };
    $scope.text11Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.iframedialog1.iconheight = $isolateScope.datavalue;
    };
    $scope.text12_2Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.iframedialog1.iconmargin = $isolateScope.datavalue;
    };

}]);